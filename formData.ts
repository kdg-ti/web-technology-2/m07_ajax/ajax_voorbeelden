document.getElementById("myForm")!
    .addEventListener("submit", (e) => {

        e.preventDefault();

        const formData = new FormData(document.getElementById("myForm") as HTMLFormElement);
        const parameters = new URLSearchParams(formData as any);

        fetch("https://jsonplaceholder.typicode.com/posts", {
            method: "post",
            body: parameters
        })
            .then(async (result) => {
                    alert("Submitted successfully!")
                    console.log(await result.json())
                }
            )
            .catch(() => console.log("Connection failed!"));
    })
