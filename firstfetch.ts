fetch('http://jsonplaceholder.typicode.com/users/1')
    .then(resp => {
        if (resp.ok) {
            return resp.json();
        } else {
            throw new Error(resp.statusText);
        }
    })
    .then(
        txt => {
            alert("antwoord: " + JSON.stringify(txt));
        })
    .catch(r => {
        alert("Bad response: " + r);
    });
