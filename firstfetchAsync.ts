surf("https://randomuser.me/api/");

async function surf(url: RequestInfo | URL) {
    const resp = await fetch(url);

    if (resp.ok) {
        alert("antwoord: " + JSON.stringify(await resp.json()));
    } else {
        alert("Probleem: " + resp.statusText);
    }
}
