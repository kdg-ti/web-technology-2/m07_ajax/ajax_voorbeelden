fetch('https://reqres.in/api/login',
    {
        method: "POST", // *GET, PUT, DELETE, etc.
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            "email": "koray.sels@kdg.be",
            "password": "mijnpaswoord"
        }),
    })
    .then(resp => {
        if (resp.ok) {
            return resp.json();
        } else {
            throw new Error(resp.statusText);
        }
    })
    .then(
        txt => {
            alert("antwoord: " + JSON.stringify(txt));
        })
    .catch(r => {
        alert("Bad response: " + r);
    });
