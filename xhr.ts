const xhr: XMLHttpRequest = new XMLHttpRequest();
xhr.open('GET', 'http://jsonplaceholder.typicode.com/users/1');
xhr.responseType = 'json';
xhr.onload = function () {
    if (this.status >= 200 && this.status < 300) {
        // Success!
        const resp = this.response;
        console.log(resp);
    } else {
        console.error("Bad response: " + this.statusText);
    }
};
xhr.onerror = () => console.error;
xhr.send();
